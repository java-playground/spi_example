package com.dictionary.impl;

import com.dictionary.spi.Dictionary;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author Vourlakis Nikolas
 */
public class ExtendedDictionary implements Dictionary {
    private SortedMap<String, String> map_;

    public ExtendedDictionary() {
        map_ = new TreeMap<String, String>();
        map_.put("XML",
                "a document standard often used in web services, among other things");
        map_.put("REST",
                "an architecture style for creating, reading, updating, " +
                "and deleting data that attempts to use the common vocabulary" +
                "of the HTTP protocol; Representational State Transfer");
    }

    public String getDefinition(String word) {
        return map_.get(word);
    }
}