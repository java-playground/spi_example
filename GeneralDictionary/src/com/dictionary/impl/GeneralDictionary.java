package com.dictionary.impl;

import com.dictionary.spi.Dictionary;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author Vourlakis Nikolas
 */
public class GeneralDictionary implements Dictionary {
    private SortedMap<String, String> map_;

    public GeneralDictionary() {
        map_ = new TreeMap<String, String>();
        map_.put("book", "a set of written or printed pages, usually bound with a protective cover");
        map_.put("editor", "a person who edits");
    }

    @Override
    public String getDefinition(String word) {
        return map_.get(word);
    }

}
