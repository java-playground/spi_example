package com.dictionary.spi;

/**
 *
 * @author Vourlakis Nikolas
 */
public interface Dictionary {
    public String getDefinition(String word);
}
