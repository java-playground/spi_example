/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dictionary.service;

/**
 *
 * @author Vourlakis Nikolas
 */
public interface DictionaryService {

    String getDefinition(String word);
}
