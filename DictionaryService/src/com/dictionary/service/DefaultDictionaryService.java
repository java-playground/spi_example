package com.dictionary.service;

import com.dictionary.spi.Dictionary;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

/**
 * Using the {@link ServiceLoader} provided by Java itself.
 *
 * @author Vourlakis Nikolas
 */
public final class DefaultDictionaryService implements DictionaryService {

    private static DictionaryService service_ = new DefaultDictionaryService();
    private ServiceLoader<Dictionary> loader_;

    private DefaultDictionaryService() {
        loader_ = ServiceLoader.load(Dictionary.class);
    }

    public static synchronized DictionaryService getInstance() {
        return service_;
    }

    @Override
    public String getDefinition(String word) {
        String definition = null;

        try {
            Iterator<Dictionary> dictionaries = loader_.iterator();
            while (definition == null && dictionaries.hasNext()) {
                Dictionary dict = dictionaries.next();
                definition = dict.getDefinition(word);
            }
        } catch (ServiceConfigurationError serviceError) {
            definition = null;
            serviceError.printStackTrace();
        }

        return definition;
    }
}
