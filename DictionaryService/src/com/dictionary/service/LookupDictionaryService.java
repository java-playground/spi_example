package com.dictionary.service;

import com.dictionary.spi.Dictionary;
import java.util.Collection;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Template;

/**
 * Using the Lookup API exposed by NetBeans  IDE.
 *
 * @author Vourlakis Nikolas
 */
public class LookupDictionaryService implements DictionaryService {

    private static DictionaryService service_ = new LookupDictionaryService();
    private Lookup lookup_;
    private Collection<Dictionary> dictionaries_;

    private LookupDictionaryService() {
        lookup_ = Lookup.getDefault();
        dictionaries_ = (Collection<Dictionary>) lookup_.lookupAll(Dictionary.class);
    }

    public static synchronized DictionaryService getInstance() {
        return service_;
    }

    @Override
    public String getDefinition(String word) {
        String definition = null;
        for (Dictionary dict : dictionaries_) {
            definition = dict.getDefinition(word);
            if (definition != null) {
                break;
            }
        }
        return definition;
    }
}
